import Service.*;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        String inFile = "data/in.xls";
        String outFile = "data/out.xls";
        String sheetName = "Лист1";

        ParseXLS parseXLS = new ParseXLS(inFile, sheetName);
        SaveFile saveFile = new SaveFile(outFile, sheetName);

    }
}
