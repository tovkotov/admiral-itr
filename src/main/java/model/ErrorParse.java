package model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorParse {
    private Employee employee;
    private String date;
    private String startTime;
    private String endTime;
}
