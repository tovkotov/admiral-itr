package model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class TimeInterval {
    private LocalDateTime startTime;
    private LocalDateTime endTime;
}
