package Service;

import lombok.Data;
import model.Employee;
import model.ErrorParse;
import model.TimeInterval;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

@Data
public class SaveFile {
    String fileName;
    String sheet;

    public SaveFile(String fileName, String sheet) throws IOException {
        this.fileName = fileName;
        this.sheet = sheet;
        save();
    }

    private void save() throws IOException {
        Workbook outWorkBook = new HSSFWorkbook();
        Sheet sheet1 = outWorkBook.createSheet(sheet);
        Sheet sheet2 = outWorkBook.createSheet("errors");
        Sheet sheetTimeSumMonth = outWorkBook.createSheet("Сумма");

        int i = 0;

        Row row = sheet1.createRow(i++);
        Cell name = row.createCell(0);
        name.setCellValue("ФИО");

        Cell start = row.createCell(1);
        start.setCellValue("ВРЕМЯ ВХОДА");

        Cell end = row.createCell(2);
        end.setCellValue("ВРЕМЯ ВЫХОДА");

        Cell duration = row.createCell(3);
        duration.setCellValue("МИН");

        for (Employee employee : TimeIntervalsService.getInstance().getEmployeeTimeIntervals().keySet()) {
            for (TimeInterval timeInterval : TimeIntervalsService.getInstance().getTimeIntervals(employee)) {
                row = sheet1.createRow(i++);

                name = row.createCell(0);
                name.setCellValue(employee.getName());

                start = row.createCell(1);
                start.setCellValue(timeInterval.getStartTime().toString());

                end = row.createCell(2);
                end.setCellValue(timeInterval.getEndTime().toString());

                duration = row.createCell(3);
                duration.setCellValue(ChronoUnit.MINUTES.between(timeInterval.getStartTime(), timeInterval.getEndTime()));
            }
        }


        i = 0;
        row = sheet2.createRow(i++);
        name = row.createCell(0);
        name.setCellValue("ФИО");

        Cell date = row.createCell(1);
        date.setCellValue("ДАТА");

        start = row.createCell(2);
        start.setCellValue("ВРЕМЯ ВХОДА");

        end = row.createCell(3);
        end.setCellValue("ВРЕМЯ ВЫХОДА");

        for (ErrorParse error : ErrorService.getInstance().getErrorList()) {
            row = sheet2.createRow(i++);
            name = row.createCell(0);
            name.setCellValue(error.getEmployee().getName());

            date = row.createCell(1);
            date.setCellValue(error.getDate());

            start = row.createCell(2);
            start.setCellValue(error.getStartTime());

            end = row.createCell(3);
            end.setCellValue(error.getEndTime());

        }

        i = 0;
        row = sheetTimeSumMonth.createRow(i++);
        name = row.createCell(0);
        name.setCellValue("ФИО");

        Cell sumMin = row.createCell(1);
        sumMin.setCellValue("Итого, в минутах");

     //   Cell sumDD = row.createCell(2);
     //   sumDD.setCellValue("Дней");

        Cell sumHH = row.createCell(3);
        sumHH.setCellValue("Итого, в часах");

        Cell sumMM = row.createCell(4);
        sumMM.setCellValue("Остаток, мин");

        for (Employee employee : TimeIntervalsService.getInstance().getEmployeeTimeIntervals().keySet()) {
            int sumMonth = 0;
            row = sheetTimeSumMonth.createRow(i++);
            name = row.createCell(0);
            name.setCellValue(employee.getName());
            for (TimeInterval timeInterval : TimeIntervalsService.getInstance().getTimeIntervals(employee)) {
                sumMonth += ChronoUnit.MINUTES.between(timeInterval.getStartTime(), timeInterval.getEndTime());
            }
            sumMin = row.createCell(1);
            sumMin.setCellValue(sumMonth);
            
            sumHH = row.createCell(3);
            sumMM = row.createCell(4);
            
            Duration d = Duration.ofMinutes(sumMonth);
            long hh = d.toHours();
            int mm = d.toMinutesPart();

            sumHH.setCellValue(hh);
            sumMM.setCellValue(mm);

        }

        outWorkBook.write(new FileOutputStream(fileName));
        outWorkBook.close();
    }
}
