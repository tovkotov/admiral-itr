package Service;

import lombok.Data;
import model.Employee;
import model.ErrorParse;

import java.util.ArrayList;
import java.util.List;

@Data
public class ErrorService {
    private static ErrorService instance;
    private List<ErrorParse> errorList;

    private ErrorService(List<ErrorParse> errorList) {
        this.errorList = errorList;
    }

    public static synchronized ErrorService getInstance() {
        if (instance == null)
            instance = new ErrorService(new ArrayList<>());
        return instance;
    }


    public void addError(Employee employee, String date, String inTime, String endTime) {
        errorList.add(new ErrorParse(
                employee,
                date,
                inTime,
                endTime
        ));
    }
}
