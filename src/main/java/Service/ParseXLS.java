package Service;

import lombok.Data;
import model.Employee;
import model.TimeInterval;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import java.io.FileInputStream;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Data
public class ParseXLS {
    private String inFile;
    private String sheetName;

    private static final int START_ROW = 8;
    private static final int EMPLOYEE_NAME_COL = 0;
    private static final int DATE_COL = 1;
    private static final int IN_TIME_COL = 4;
    private static final int END_TIME_COL = 5;

    private static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm";
    private static final String TIME_PATTERN = "HH:mm";

    public ParseXLS(String inFile, String sheetName) {
        this.inFile = inFile;
        this.sheetName = sheetName;
        read();
    }

    private void read() {
        int count = 0;
        try {
            FileInputStream file = new FileInputStream(inFile);
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheet(sheetName);

            Employee employee = null;
            String dateString = null;

            for (Row row : sheet) {
                count++;
                if (row.getRowNum() < START_ROW) continue;
                if (row.getCell(IN_TIME_COL).getStringCellValue().isEmpty()) {
                    System.out.println("обработано строк: " + count);
                    break;
                }

                String parseEmployeeString = row.getCell(EMPLOYEE_NAME_COL).getStringCellValue();

                employee = parseEmployeeString.isEmpty() ? employee :
                        EmployeeService.getInstance().
                                getEmployee(row.getCell(EMPLOYEE_NAME_COL).getStringCellValue());

                String parseDate = row.getCell(DATE_COL).getStringCellValue();
                dateString = parseDate.isEmpty() ? dateString : parseDate;

                String inTimeString = getTime(row.getCell(IN_TIME_COL));
                String endTimeString = getTime(row.getCell(END_TIME_COL));

                if (inTimeString == null) continue;
                if (endTimeString == null) continue;

                if (inTimeString.equals("(нет)") || endTimeString.equals("(нет)")) {
                    ErrorService.getInstance().addError(employee, dateString, inTimeString, endTimeString);
                    continue;
                }

                DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(DATE_TIME_PATTERN);
                DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern(TIME_PATTERN);

                LocalTime workStartTime = LocalTime.of(7, 20);
                LocalTime timeFromPlant = LocalTime.parse(inTimeString, timeFormatter);

                LocalDateTime startTime;

                if (timeFromPlant.isBefore(workStartTime)){
                    startTime = LocalDateTime.parse(dateString + " 07:20", dateTimeFormatter);
                } else
                    startTime = LocalDateTime.parse(dateString + " " + inTimeString, dateTimeFormatter);

                LocalDateTime endTime = LocalDateTime.parse(dateString + " " + endTimeString, dateTimeFormatter);

                List<TimeInterval> timeIntervalArrayList = TimeIntervalsService.getInstance().getTimeIntervals(employee);
                timeIntervalArrayList.add(new TimeInterval(startTime, endTime));

                TimeIntervalsService.getInstance().getEmployeeTimeIntervals().put(employee, timeIntervalArrayList);

            }
        } catch (Exception e) {
            System.out.println("Ошибка в строке: " + count);
            e.printStackTrace();
        }
    }

    private String getTime(Cell cell) {
        String tmp = cell.getStringCellValue();
        if (tmp.isEmpty()) return null;
        if (tmp.equals("не зафиксирован")) return null;
        return tmp.substring(0, 5);
    }
}
